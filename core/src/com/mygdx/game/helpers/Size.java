package com.mygdx.game.helpers;

public class Size {
    // Atributos
    private float width;
    private float height;

    // Constructor
    public Size(float newWidth, float newHeight) {
        width = newWidth;
        height = newHeight;
    }
    // Metodos
    public float getWidth() { return width; }

    public float  getHeight() { return height; }

    public void setSize(float newWidth, float newHeight)
    {
        width = newWidth;
        height = newHeight;
    }

}
